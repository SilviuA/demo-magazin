#pragma once

#include <iostream>
#include <fstream>

using namespace std;

struct int2
{
	int lg, lt;
};
class produs
{
	int nr;
	double pret;
public:
	void nrSet(int nr) { this->nr = nr; }
	void setPret(double pret) { this->pret = pret; }
	double getPret() { return pret; }
	int getNr() { return nr; }
	void schimbaPret(int pretNou);
	void schimbaNr(int nrNou);
	virtual void afisare(ostream&) = 0;
	virtual void afisareXML(ostream&) = 0;
};
class procesor:public produs
{
	char *producator;
	int nrNuclee, frecventa;
public:
	procesor();
	procesor(char* ,int ,int);
	procesor(procesor&);
	void afisare(ostream&);
	void afisareXML(ostream&);
	friend procesor* procesorCitesteXML(istream&);
	friend procesor* adaugaProcesor(istream&);
};
class display :public produs
{
	char *tip;
	float diagonala;
	int2 rezolutie;
public:
	display();
	display(char*, float, int2);
	display(display&);
	void afisare(ostream&);
	void afisareXML(ostream&);
	friend display* displayCitesteXML(istream&);
	friend display* adaugaDisplay(istream&);
};

class memorie :public produs
{
	char *tip;
	int ram;
	int frecventa;
public:
	memorie();
	memorie(char*, int, int);
	memorie(memorie&);
	void afisare(ostream&);
	void afisareXML(ostream&);
	friend memorie* memorieCitesteXML(istream&);
	friend memorie* adaugaMemorie(istream&);
};

class HardDisk :public produs
{
	char *tip;
	int capacitate;
public:
	HardDisk();
	HardDisk(char*,int);
	HardDisk(HardDisk&);
	void afisare(ostream& );
	void afisareXML(ostream&);
	friend HardDisk* HardDiskCitesteXML(istream&);
	friend HardDisk* adaugaHardDisk(istream&);
};

class placaVideo :public produs
{
	char *tip;
	char *producator;
	char *seria;
public:
	placaVideo();
	placaVideo(char*, char*, char*);
	placaVideo(placaVideo&);
	void afisare(ostream&);
	void afisareXML(ostream&);
	friend placaVideo* placaVideoCitesteXML(istream&);
	friend placaVideo* adaugaPlacaVideo(istream&);
};

class laptop :public produs //,public procesor, public placaVideo, public HardDisk, public memorie, public display
{
	char *producator;
	char *tip;
	procesor *lprocesor;
	placaVideo *lplacaVideo;
	HardDisk *lHardDisk;
	memorie *lmemorie;
	display *ldisplay;

public:
	void afisare(ostream&);
	void afisareXML(ostream&);
	laptop(char*,char*,procesor,placaVideo,HardDisk,memorie,display);// : procesor(), placaVideo(), HardDisk(), memorie(), display() {}
	friend laptop* laptopCitesteXML(istream&);
	friend laptop* adaugaLaptop(istream&);
};

////////// DE COMPLETAT CU ASTEA!!!
/*
class telefon :public produs//, public procesor, public placaVideo, public HardDisk, public memorie, public display
{
public:

};*/


/* class CASTI -> BOXE !!!!!! */
