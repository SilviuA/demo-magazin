#pragma once
#include <iostream>
#include "produse.h"
#include "Tree.h"

using namespace std;
#define dmax 20

struct Hash
{
	nod* rad[dmax];
};

void initHash();
nod* adaugaProdus(produs*, int cod);
