#pragma once

struct nod
{
	int data, height;
	nod *st, *dr;
};

void InsAVL(nod *&a, int x);
int adancime(nod *r);
void afisare2d(nod *r);
