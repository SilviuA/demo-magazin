#include <string.h>
#include <conio.h>
#include "produse.h"

//////////// CHESTII

void flush(FILE* pFile)
{
	char gb='1';
	while (gb != '\n') gb = getc(pFile);
}

 //////////////// PROCESOR
procesor::procesor()
{
	producator = 0;
	nrNuclee = -1;
	frecventa = -1;
}

procesor::procesor(char *producator, int nrNuclee, int frecventa)
{
	int lg = strlen(producator)+ 1;
	this->producator = new char[lg];
	strcpy_s(this->producator,lg, producator);
	this->nrNuclee = nrNuclee;
	this->frecventa = frecventa;
}

procesor::procesor(procesor &a)
{
	int lg = strlen(a.producator)+1;
	this->producator = new char[lg];
	strcpy_s(this->producator, lg, a.producator);
	this->nrNuclee = a.nrNuclee;
	this->frecventa = a.frecventa;
}

void procesor::afisare(ostream &cout)
{
	//cout << "PROCESOR:" << endl;
	cout << " producator = " << producator << endl;
	cout << " nrNuclee = " << nrNuclee << endl;
	cout << " frecventa = " << frecventa << endl;
	cout << " nrProd = " << this->getNr() << endl;
	cout << " pret = " << this->getPret() << endl;
	cout << endl;
}

void procesor::afisareXML(ostream &cout)
{
	//setXML(cout);
	cout << "\n<PROCESOR>" << endl;
	cout << "<PRODUCATOR> " << producator << " </PRODUCATOR>" << endl;
	cout << "<nrNUCLEE> " << nrNuclee << " </nrNUCLEE>" << endl;
	cout << "<FRECVENTA> " << frecventa << " </FRECVENTA>" << endl;
	cout << "<nrPRODUSE> " << this->getNr() << " </nrPRODUSE>" << endl;
	cout << "<PRET> " << this->getPret() << " </PRET>" << endl;
	cout << "</PROCESOR>";
}
procesor * procesorCitesteXML(istream &cin)
{
	if (!cin.eof()) {
		char garbage[30];
		char producator1[30];
		int nrNuclee, frecventa;
		cin >> garbage >> garbage;
		cin >> producator1;
		cin >> garbage >> garbage;
		cin >> nrNuclee;
		cin >> garbage >> garbage;
		cin >> frecventa;
		cin >> garbage >> garbage;
		double pret;
		int nr;
		cin >> nr;
		cin >> garbage >> garbage;
		cin >> pret;
		cin >> garbage >> garbage;
		procesor* sol = new procesor(producator1, nrNuclee, frecventa);
		sol->nrSet(nr);
		sol->setPret(pret);
		return sol; /// DE TERMINAT // AFISARE/CITIRE  PRET SI ALTE CHESTII
	}
	return 0;
}
//////////////// DISPLAY
display::display()
{
	tip = 0;
	diagonala = 0;
	rezolutie.lg = rezolutie.lt = 0;
}

display::display(char *tip, float diagonala, int2 rezolutie)
{
	int lg = strlen(tip) + 1;
	this->tip = new char[lg];
	strcpy_s(this->tip,lg, tip);
	this->diagonala = diagonala;
	this->rezolutie.lg = rezolutie.lg;
	this->rezolutie.lt = rezolutie.lt;
}

display::display(display &a)
{
	int lg = strlen(a.tip) + 1;
	this->tip = new char[lg];
	strcpy_s(this->tip, lg, a.tip);
	this->diagonala = a.diagonala;
	this->rezolutie = a.rezolutie;
}

void display::afisare(ostream& cout)
{
	//cout << "DISPLAY" << endl;
	cout << "tip = " << tip << endl;
	cout << "diagonala = " << diagonala << endl;
	cout << "rezolutie = " << rezolutie.lg << "X" << rezolutie.lt << endl;
	cout << " nrProd = " << this->getNr() << endl;
	cout << " pret = " << this->getPret() << endl;
	cout << endl;
}

void display::afisareXML(ostream &cout)
{
	//setXML(cout);
	cout << "\n<DISPLAY>" << endl;
	cout << "<TIP> " << tip << " </TIP>" << endl;
	cout << "<DIAGONALA> " << diagonala << " </DIAGONALA>" << endl;
	cout << "<REZOLUTIE> " << rezolutie.lg << " X " << rezolutie.lt << " </REZOLUTIE>" << endl;
	cout << "<nrPRODUSE> " << this->getNr() << " </nrPRODUSE>" << endl;
	cout << "<PRET> " << this->getPret() << " </PRET>" << endl;
	cout << "</DISPLAY>";
}

display * displayCitesteXML(istream &cin)
{
	char tip[30];
	float diagonala;
	int2 rezolutie;
	char garbage[30];
	cin >> garbage >> garbage;
	cin >> tip;
	cin >> garbage >> garbage;
	cin >> diagonala;
	cin >> garbage >> garbage;
	cin >> rezolutie.lg;
	_getch(); // pt X`ul de la rezolutie (1200x1400)
	cin >> rezolutie.lt;
	cin >> garbage >> garbage;
	double pret;
	int nr;
	cin >> nr;
	cin >> garbage >> garbage;
	cin >> pret;
	cin >> garbage >> garbage;
	display* sol = new display(tip, diagonala, rezolutie);
	sol->nrSet(nr);
	sol->setPret(pret);
	return sol;
}
//////////////// MEMORIE 
memorie::memorie()
{
	tip = 0;
	ram = frecventa = -1;
}

memorie::memorie(char *tip, int ram, int frecventa)
{
	int lg = strlen(tip) + 1;
	this->tip = new char[lg];
	strcpy_s(this->tip,lg,tip);
	this->ram = ram;
	this->frecventa = frecventa;
}

memorie::memorie(memorie &a)
{
	int lg = strlen(a.tip) + 1;
	this->tip = new char[lg];
	strcpy_s(this->tip, lg, a.tip);
	this->ram = a.ram;
	this->frecventa = a.frecventa;
}

void memorie::afisare(ostream& cout)
{
	//cout << "MEMORIE!" << endl;
	cout << "tip = " << tip;
	cout << "ram = " << ram << endl;
	cout << "frecventa = " << frecventa << endl;
	cout << " nrProd = " << this->getNr() << endl;
	cout << " pret = " << this->getPret() << endl;
	cout << endl;
}

void memorie::afisareXML(ostream &cout)
{
	//setXML(cout);
	cout << "\n<MEMORIE>" << endl;
	cout << "<TIP> " << tip << " </TIP>" << endl;
	cout << "<RAM> " << ram << " </RAM>" << endl;
	cout << "<FRECVENTA> " << frecventa << " </FRECVENTA>" << endl;
	cout << "<nrPRODUSE> " << this->getNr() << " </nrPRODUSE>" << endl;
	cout << "<PRET> " << this->getPret() << " </PRET>" << endl;
	cout << "</MEMORIE>";
}
memorie * memorieCitesteXML(istream &cin)
{
	char tip[30];
	int ram;
	int frecventa;
	char garbage[30];
	cin >> garbage >> garbage;
	cin >> tip;
	cin >> garbage >> garbage;
	cin >> ram;
	cin >> garbage >> garbage;
	cin >> frecventa;
	cin >> garbage >> garbage;
	double pret;
	int nr;
	cin >> nr;
	cin >> garbage >> garbage;
	cin >> pret;
	cin >> garbage >> garbage;
	memorie* sol = new memorie(tip, ram, frecventa);
	sol->nrSet(nr);
	sol->setPret(pret);
	return sol;
}
//////////////// HARD DISK
HardDisk::HardDisk()
{
	tip = 0;
	capacitate = -1;
}

HardDisk::HardDisk(char *tip, int capacitate)
{
	int lg = strlen(tip) + 1;
	this->tip = new char[lg];
	strcpy_s(this->tip, lg, tip);
	this->capacitate = capacitate;
}

HardDisk::HardDisk(HardDisk &a)
{
	int lg = strlen(a.tip) + 1;
	this->tip = new char[lg];
	strcpy_s(this->tip, lg, a.tip);
	this->capacitate = a.capacitate;
}

void HardDisk::afisare(ostream& cout)
{
	//cout << "HardDisk" << endl;
	cout << "tip = " << tip;
	cout << "capacitate = " << capacitate << endl;
	cout << " nrProd = " << this->getNr() << endl;
	cout << " pret = " << this->getPret() << endl;
	cout << endl;
}

void HardDisk::afisareXML(ostream &cout)
{
	//setXML(cout);
	cout << "\n<HARDDISK>" << endl;
	cout << "<TIP> " << tip << " </TIP>" << endl;
	cout << "<CAPACITATE> " << capacitate << " </CAPACITATE>" << endl;
	cout << "<nrPRODUSE> " << this->getNr() << " </nrPRODUSE>" << endl;
	cout << "<PRET> " << this->getPret() << " </PRET>" << endl;
	cout << "</HARDDISK>";
}

HardDisk * HardDiskCitesteXML(istream &cin)
{
	char tip[30];
	int capacitate;
	char garbage[30];
	cin >> garbage >> garbage;
	cin >> tip;
	cin >> garbage >> garbage;
	cin >> capacitate;
	cin >> garbage >> garbage;
	double pret;
	int nr;
	cin >> nr;
	cin >> garbage >> garbage;
	cin >> pret;
	cin >> garbage >> garbage;
	HardDisk* sol = new HardDisk(tip, capacitate);
	sol->setPret(pret);
	sol->nrSet(nr);
	return sol;
}



/////////////// PLACA VIDEO
placaVideo::placaVideo()
{
	tip = producator = seria = 0;
}

placaVideo::placaVideo(char *tip, char *producator, char *seria)
{
	int lg = strlen(tip) + 1;
	this->tip = new char[lg];
	strcpy_s(this->tip, lg, tip);
	lg = strlen(producator) + 1;
	this->producator = new char[lg];
	strcpy_s(this->producator, lg, producator);
	lg = strlen(seria) + 1;
	this->seria = new char[lg];
	strcpy_s(this->seria, lg, seria);
}

placaVideo::placaVideo(placaVideo &a)
{
	int lg = strlen(a.tip) + 1;
	this->tip = new char[lg];
	strcpy_s(this->tip, lg, a.tip);
	lg = strlen(a.producator) + 1;
	this->producator = new char[lg];
	strcpy_s(this->producator, lg, a.producator);
	lg = strlen(a.seria) + 1;
	this->seria = new char[lg];
	strcpy_s(this->seria, lg, a.seria);
}

void placaVideo::afisare(ostream& cout)
{
	//cout << "PLACA VIDEO" << endl;
	cout << "tip = " << tip;
	cout << "producator = " << producator << endl;
	cout << "seria = " << seria << endl;
	cout << " nrProd = " << this->getNr() << endl;
	cout << " pret = " << this->getPret() << endl;
	cout << endl;//
}

void placaVideo::afisareXML(ostream &cout)
{
	//setXML(cout);
	cout << "\n<PlacaVideo>" << endl;
	cout << "<TIP> " << tip << " </TIP>" << endl;
	cout << "<PRODUCATOR> " << producator << " </PRODUCATOR>" << endl;
	cout << "<SERIA> " << seria << " </SERIA>" << endl;
	cout << "<nrPRODUSE> " << this->getNr() << " </nrPRODUSE>" << endl;
	cout << "<PRET> " << this->getPret() << " </PRET>" << endl;
	cout << "</PlacaVideo>";
}

placaVideo * placaVideoCitesteXML(istream &cin)
{
	char tip[30];
	char producator[30];
	char seria[30];
	char garbage[30];
	cin >> garbage >> garbage;
	cin >> tip;
	cin >> garbage >> garbage;
	cin >> producator;
	cin >> garbage >> garbage;
	cin >> seria;
	cin >> garbage >> garbage;
	double pret;
	int nr;
	cin >> nr;
	cin >> garbage >> garbage;
	cin >> pret;
	cin >> garbage >> garbage;
	placaVideo* sol = new placaVideo(tip, producator, seria);
	sol->nrSet(nr);
	sol->setPret(pret);
	return sol;
}

/******  cateva FUNCTII DE CITIRE    *********/
procesor *adaugaProcesor(istream& pFile)
{
	char producator1[30];
	int nrNuclee, frecventa;
	procesor *sol;
	if(&pFile==&cin) cout << "producator = ";
	pFile >> producator1;
	if (&pFile == &cin) cout << "nr nuclee = ";
	pFile >> nrNuclee;
	if (&pFile == &cin) cout << "frecventa = ";
	pFile >> frecventa;
	sol = new procesor(producator1, nrNuclee, frecventa); 
	//flush(pFile);
	double pret;
	int nrprod;
	if (&pFile == &cin) cout << "Pretul produsului = ";
	cin >> pret;
	if (&pFile == &cin) cout << "Numarul de produse aflate in stoc = ";
	cin >> nrprod;
	sol->setPret(pret);
	sol->nrSet(nrprod);
	return sol;
}

display *adaugaDisplay(istream& pFile)
{
	display *sol;
	char tip[30];
	float diagonala;
	int2 rezolutie;
	if (&pFile == &cin) cout << "tip = ";
	pFile >> tip;
	if (&pFile == &cin) cout << "diagonala = ";
	pFile >> diagonala;
	if (&pFile == &cin) cout << "rezolutie = (lg lt)";
	pFile >> rezolutie.lg >> rezolutie.lt;
	sol = new display(tip, diagonala, rezolutie);
	double pret;
	int nrprod;
	if (&pFile == &cin) cout << "Pretul produsului = ";
	cin >> pret;
	if (&pFile == &cin) cout << "Numarul de produse aflate in stoc = ";
	cin >> nrprod;
	sol->setPret(pret);
	sol->nrSet(nrprod);
	return sol;
}

memorie * adaugaMemorie(istream& pFile)
{
	memorie *sol;
	char tip[30];
	int ram, frecventa;
	if (&pFile == &cin) cout << "tip = ";
	pFile >> tip;
	if (&pFile == &cin) cout << "ram = ";
	pFile >> ram;
	if (&pFile == &cin) cout << "frecventa = ";
	pFile >> frecventa;
	sol = new memorie(tip, ram, frecventa);
	double pret;
	int nrprod;
	if (&pFile == &cin) cout << "Pretul produsului = ";
	cin >> pret;
	if (&pFile == &cin) cout << "Numarul de produse aflate in stoc = ";
	cin >> nrprod;
	sol->setPret(pret);
	sol->nrSet(nrprod);
	//flush(pFile);
	return sol;
}

HardDisk * adaugaHardDisk(istream& pFile)
{
	HardDisk *sol;
	char tip[30];
	int capacitate;
	if (&pFile == &cin) cout << "tip = ";
	pFile >> tip;
	if (&pFile == &cin) cout << "capacitate = ";
	pFile >> capacitate;
	sol = new HardDisk(tip, capacitate);
	double pret;
	int nrprod;
	if (&pFile == &cin) cout << "Pretul produsului = ";
	cin >> pret;
	if (&pFile == &cin) cout << "Numarul de produse aflate in stoc = ";
	cin >> nrprod;
	sol->setPret(pret);
	sol->nrSet(nrprod);
	//flush(pFile);
	return sol;
}


placaVideo * adaugaPlacaVideo(istream& pFile)
{
	placaVideo *sol;
	char tip[30], producator[30], seria[30];
	if (&pFile == &cin) cout << "tip = ";
	pFile >> tip;
	if (&pFile == &cin) cout << "producator = ";
	pFile >> producator;
	if (&pFile == &cin) cout << "seria = ";
	pFile >> seria;
	sol = new placaVideo(tip, producator, seria);
	double pret;
	int nrprod;
	if (&pFile == &cin) cout << "Pretul produsului = ";
	cin >> pret;
	if (&pFile == &cin) cout << "Numarul de produse aflate in stoc = ";
	cin >> nrprod;
	sol->setPret(pret);
	sol->nrSet(nrprod);
	return sol;
}

laptop * adaugaLaptop(istream& pFile)
{
	procesor *p1;
	display *p2;
	memorie *p3;
	HardDisk *p4;
	placaVideo *p5;
	char producator[50],tip[50];
	laptop *sol;
	if(&pFile == &cin) cout << "PROCESOR" << endl;
	p1=adaugaProcesor(pFile);
	if (&pFile == &cin) cout << "DISPLAY" << endl;
	p2=adaugaDisplay(pFile);
	if (&pFile == &cin) cout << "MEMORIE" << endl;
	p3=adaugaMemorie(pFile);
	if (&pFile == &cin) cout << "HardDisK" << endl;
	p4=adaugaHardDisk(pFile);
	if (&pFile == &cin) cout << "Placa Video" << endl;
	p5=adaugaPlacaVideo(pFile);
	if (&pFile == &cin) cout << "PRODUCATORUL LAPTOPULUI = ";
	pFile >> producator;
	if (&pFile == &cin) cout << "Tipul laptopului = ";
	pFile >> tip;
	double pret;
	int nrprod;
	if (&pFile == &cin) cout << "Pretul produsului = ";
	cin >> pret;
	if (&pFile == &cin) cout << "Numarul de produse aflate in stoc = ";
	cin >> nrprod;
	sol = new laptop(producator, tip, *p1, *p5, *p4, *p3, *p2);
	sol->setPret(pret);
	sol->nrSet(nrprod);
	delete p1;
	delete p2;
	delete p3;
	delete p4;
	delete p5;
	return sol;
}

void laptop::afisare(ostream &cout)
{
	//////// DE TERMINAT SI ASTA!
	//cout << "LAPTOP" << endl;
	cout << "Producator = " << producator << endl;
	cout << "Tip = " << tip << endl;
	this->lprocesor->afisare(cout);
	this->lHardDisk->afisare(cout);
	this->lmemorie->afisare(cout);
	this->lplacaVideo->afisare(cout);
	this->lprocesor->afisare(cout);
	cout << "\n nrProd = " << this->getNr() << endl;
	cout << " pret = " << this->getPret() << endl;
	cout << endl;
}

void laptop::afisareXML(ostream &cout)
{
	//setXML(cout);
	cout << "\n<LAPTOP>" << endl;
	cout << "<PRODUCATOR> " << producator << " </PRODUCATOR>" << endl;
	cout << "<TIP> " << tip << " </TIP>" << endl;
	this->lprocesor->afisareXML(cout);
	this->lHardDisk->afisareXML(cout);
	this->lmemorie->afisareXML(cout);
	this->lplacaVideo->afisareXML(cout);
	this->ldisplay->afisareXML(cout);
	cout << "<nrPRODUSE> " << this->getNr() << " </nrPRODUSE>" << endl;
	cout << "<PRET> " << this->getPret() << " </PRET>" << endl;
	cout << "</LAPTOP>";
}

laptop * laptopCitesteXML(istream &cin)
{
	char producator[30];
	char tip[30];
	procesor *lprocesor;
	placaVideo *lplacaVideo;
	HardDisk *lHardDisk;
	memorie *lmemorie;
	display *ldisplay;
	char garbage[30];
	cin >> garbage >> garbage;
	cin >> producator;
	cin >> garbage >> garbage;
	cin >> tip;
	lprocesor = new procesor(*procesorCitesteXML(cin));
	lHardDisk = new HardDisk(*HardDiskCitesteXML(cin));
	lmemorie = new memorie(*memorieCitesteXML(cin));
	lplacaVideo = new placaVideo(*placaVideoCitesteXML(cin));
	ldisplay = new display(*displayCitesteXML(cin));
	cin >> garbage;
	double pret;
	int nr;
	cin >> nr;
	cin >> garbage >> garbage;
	cin >> pret;
	cin >> garbage >> garbage;
	laptop* sol = new laptop(producator, tip, *lprocesor, *lplacaVideo, *lHardDisk, *lmemorie, *ldisplay);
	sol->setPret(pret);
	sol->nrSet(nr);
	return sol;
}

laptop::laptop(char *producator, char *tip, procesor a, placaVideo b, HardDisk c, memorie d, display e)
{
	int lg = strlen(producator)+1;
	this->producator = new char[lg];
	strcpy_s(this->producator, lg, producator);
	lg = strlen(tip)+1;
	this->tip = new char[lg];
	strcpy_s(this->tip, lg, tip);
	this->lprocesor = new procesor(a);
	this->lplacaVideo = new placaVideo(b);
	this->lHardDisk = new HardDisk(c);
	this->lmemorie = new memorie(d);
	this->ldisplay = new display(e);
}

void produs::schimbaPret(int pretNou)
{
	this->pret = pretNou;
}

void produs::schimbaNr(int nrNou)
{
	nr = nrNou;
}

