#pragma once
#include "Menu.h"
#include "myStore.h"
#include "Tree.h"

///// AFISAREA UNUI ARBORE DIN Hash
class AfisareSTD : public Operation
{
	int cod;
public:
	AfisareSTD(char *name,int cod);
	void ExecuteOperation(void);
};
class AfisareDescrescatoare :public Operation
{
	int cod;
public:
	AfisareDescrescatoare(char *name, int cod);
	void ExecuteOperation(void);
};

///// ADAUGA ELEMENT
class AdaugaElem : public Operation
{
	int cod;
public:
	AdaugaElem(char *name, int cod);
	void ExecuteOperation(void);
};
 //// DESCARCA BAZA DE DATE
class DescarcaBazaDeDate :public Operation
{
public:
	DescarcaBazaDeDate(char* name);
	void ExecuteOperation(void);
};
//// CITESTE BAZA DE DATE
class CitesteBazaDeDate :public Operation
{
public:
	CitesteBazaDeDate(char* name);
	void ExecuteOperation(void);
};


///// STERGE PRODUS
class StergeProdus :public Operation
{
	int cod;
public:
	StergeProdus(char* name,int cod);
	void ExecuteOperation(void);
};
//// MODIFICA PRETUL UNUI PRODUS
class ModificaPret :public Operation
{
	int cod;
public:
	ModificaPret(char *name, int cod);
	void ExecuteOperation(void);
};

///// CUMPARA PRODUS
class CumparaProdus :public Operation
{
	int cod;
public:
	CumparaProdus(char *name, int cod);
	void ExecuteOperation(void);
};

//// STERGE BAZA DE DATE
class StergeBazaDeDate : public Operation
{
public:
	StergeBazaDeDate(char*name) :Operation(name) {}
	void ExecuteOperation(void); //////// DE TERMINAT CHESTIA ASTA
};