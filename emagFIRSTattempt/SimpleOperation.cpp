#include <iostream>
#include <fstream>
#include <conio.h>
#include "Globals.h"
#include "SimpleOperation.h"
using namespace std;
extern Hash H;

/******* CHESTII CARE POT SAU AR PUTEA AJUTA *******/
void setXML(ostream& cout)
{
	cout << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
}

/****** Membri clasa AfisareSTD ******/
AfisareSTD::AfisareSTD(char *name, int cod) : Operation(name)
{
	this->cod = cod;
}

void AfisareSTD::ExecuteOperation(void)
{
	showGreaterOrder(H.rad[this->cod],cout);
	Pause();
}

/****** Membri clasa AdaugaElem ******/
AdaugaElem::AdaugaElem(char *name, int cod) : Operation(name)
{
	this->cod = cod;
}

void AdaugaElem::ExecuteOperation(void)
{
	switch (cod)
	{
	case 0:
		adaugaProdus(adaugaProcesor(cin), 0);
		break;
	case 1:
		adaugaProdus(adaugaDisplay(cin), 1);
		break;
	case 2:
		adaugaProdus(adaugaMemorie(cin), 2);
		break;
	case 3:
		adaugaProdus(adaugaHardDisk(cin), 3);
		break;
	case 4:
		adaugaProdus(adaugaPlacaVideo(cin), 4);
		break;
	case 5:
		adaugaProdus(adaugaLaptop(cin), 5);
		break;
	default:
		cout << "Nimic!";
	}
	Pause();
}


/******  MEMBRI CLASA DescarcaBazaDeDate   *****/

DescarcaBazaDeDate::DescarcaBazaDeDate(char * name) : Operation(name)
{
}

void DescarcaBazaDeDate::ExecuteOperation(void)
{
	cout << "APASATI TASTA 1 PENTRU A CONTINUA ACTIUNEA ";
	char ch = _getch();
	if (ch == '1') {
		ofstream out("procesor.xml");
		setXML(out);
		out << "<INFO>" << endl;
		descarcaXML(H.rad[0], out);
		out << "\n</INFO>" << endl;
		out.close();

		out.open("display.xml");
		setXML(out);
		out << "<INFO>" << endl;
		descarcaXML(H.rad[1], out);
		out << "\n</INFO>" << endl;
		out.close();

		out.open("memorie.xml");
		setXML(out);
		out << "<INFO>" << endl;
		descarcaXML(H.rad[2], out);
		out << "\n</INFO>" << endl;

		out.close();
		out.open("harddisk.xml");
		setXML(out);
		out << "<INFO>" << endl;
		descarcaXML(H.rad[3], out);
		out << "\n</INFO>" << endl;
		out.close();

		out.open("PlacaVideo.xml");
		setXML(out);
		out << "<INFO>" << endl;
		descarcaXML(H.rad[4], out);
		out << "\n</INFO>" << endl;
		out.close();

		out.open("laptop.xml");
		setXML(out);
		out << "<INFO>" << endl;
		descarcaXML(H.rad[5], out);
		out << "\n</INFO>" << endl;
		out.close();
		std::cout << "\nBAZA DE DATE A FOST SALVATA!";
		_getch();
	}
	else
	{
		std::cout << "\nACTIUNEA A FOST ANULATA!";
		_getch();
	}
}

CitesteBazaDeDate::CitesteBazaDeDate(char * name) : Operation(name)
{
}

void CitesteBazaDeDate::ExecuteOperation(void) /// TREBUIE FACUTE AFISARILE XML CALUMEA(cu pret) 
{
	char garbage[30];
	ifstream in("procesor.xml");
	in >> garbage >> garbage >> garbage;
	in >> garbage >> garbage;
	while (strcmp(garbage,"<nrCRT>")==0)
	{
		in >> garbage >> garbage;
		adaugaProdus(procesorCitesteXML(in), 0);
		in >> garbage;
	}
	in.close();

	in.open("display.xml");
	in >> garbage >> garbage >> garbage;
	in >> garbage >> garbage;
	while (strcmp(garbage, "<nrCRT>")==0)
	{
		in >> garbage >> garbage;
		adaugaProdus(displayCitesteXML(in), 1);
		in >> garbage;
	}
	in.close();

	in.open("memorie.xml");
	in >> garbage >> garbage >> garbage;
	in >> garbage >> garbage;
	while (strcmp(garbage, "<nrCRT>")==0)
	{
		in >> garbage >> garbage;
		adaugaProdus(memorieCitesteXML(in), 2);
		in >> garbage;
	}
	in.close();

	in.open("harddisk.xml");
	in >> garbage >> garbage >> garbage;
	in >> garbage >> garbage;
	while (strcmp(garbage, "<nrCRT>")==0)
	{
		in >> garbage >> garbage;
		adaugaProdus(HardDiskCitesteXML(in), 3);
		in >> garbage;
	}
	in.close();

	in.open("PlacaVideo.xml");
	in >> garbage >> garbage >> garbage;
	in >> garbage >> garbage;
	while (strcmp(garbage, "<nrCRT>")==0)
	{
		in >> garbage >> garbage;
		adaugaProdus(placaVideoCitesteXML(in), 4);
		in >> garbage;
	}
	in.close();

	in.open("laptop.xml");
	in >> garbage >> garbage >> garbage;
	in >> garbage >> garbage;
	while (strcmp(garbage, "<nrCRT>")==0)
	{
		in >> garbage >> garbage;
		adaugaProdus(laptopCitesteXML(in), 5);
		in >> garbage;
	}
	in.close();

}

AfisareDescrescatoare::AfisareDescrescatoare(char * name, int cod) :Operation(name)
{
	this->cod = cod;
}

void AfisareDescrescatoare::ExecuteOperation(void)
{
	showLowerOrder(H.rad[this->cod], cout);
	Pause();
}

StergeProdus::StergeProdus(char * name, int cod):Operation(name)
{
	this->cod = cod;
}

void StergeProdus::ExecuteOperation(void)
{
	int nr;
	cout << "Introdu nrCRT al produsului pe care doriti sa il stergeti!\n -> ";
	cin >> nr;
	nod* sol = search(H.rad[cod], nr);
	deleteNode(H.rad[cod], sol->data);
	Pause();
}

ModificaPret::ModificaPret(char * name, int cod):Operation(name)
{
	this->cod = cod;
}

void ModificaPret::ExecuteOperation(void)
{
	nod* sol;
	nod* readauga;
	int nr;
	int pretNou;
	cout << "Introduceti nrCRT al produsului pe care il doriti sters.\n-> ";
	cin >> nr;
	cout << "Introduceti noul pret!\n-> ";
	cin >> pretNou; ///trebuie facuta o copie a produsului si apoi adaugata cu alt pret
	sol = search(H.rad[cod], nr);
	readauga = new nod(*sol);
	deleteNode(H.rad[cod], sol->data);
	readauga->data->schimbaPret(pretNou);
	insert(H.rad[cod], readauga->data);
	Pause();
}

CumparaProdus::CumparaProdus(char * name, int cod):Operation(name)
{
	this->cod = cod;
}

void CumparaProdus::ExecuteOperation(void)
{
	int nrprod;
	produs* prod;
	cout << "Introduceti nrCRT al produsului de achizitionat!\n-> ";
	cin >> nrprod;
	prod = search(H.rad[cod], nrprod)->data;
	while (!prod)
	{
		cout << "Nu exista acest produs!\n";
		cout << "Introduceti nrCRT al produsului de achizitionat!\n-> ";
		cin >> nrprod;
		prod = search(H.rad[cod], nrprod)->data;
	}
	cout << "Cate produse doriti sa achizitionati?\n-> ";
	cin >> nrprod; 
	while (nrprod > prod->getNr())
	{
		cout << "Nu sunt suficiente produse.Incercati din nou!\n-> ";
		cin >> nrprod;
	}
	prod->schimbaNr((prod->getNr() - nrprod));
	Pause();
}

