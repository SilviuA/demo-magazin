﻿#include <iostream>
#include <deque>
#include "AVL.h"
using namespace std;

//////////// AFISARE 2D <<<

int nrt, nrv;
void parcurge_adancime(nod *r)
{
	if (r) {
		nrt++;
		if (nrt > nrv) nrv = nrt;
		parcurge_adancime(r->st);
		parcurge_adancime(r->dr);
		nrt--;
	}
}
int adancime(nod * r)
{
	nrt = 0;
	nrv = 0;
	parcurge_adancime(r);
	return nrv;
}

int lvl, nrsp, k;
deque <nod*> q;

void afisare2d()
{
	lvl++;
	k *= 2;
	nod *aux;

	for (int j = 0; j < nrsp / k; j++)
		cout << " ";

	if (lvl <= nrv) {
		for (int i = 0; !q.empty() && i < k / 2; i++)
		{
			aux = q.front();
			q.pop_front();

			////
			if (aux&&aux->st)
			{
				cout << aux->st->data;
				q.push_back(aux->st);
			}
			else
			{
				cout << "-";
				q.push_back(0);
			}
			//////
			for (int j = 0; j <2 * nrsp / k; j++)
				cout << " ";
			if (aux&&aux->dr)
			{
				cout << aux->dr->data;
				q.push_back(aux->dr);
			}
			else
			{
				cout << "-";
				q.push_back(0);
			}
			for (int j = 0; j < 2 * nrsp / k; j++)
				cout << " ";

		}
		cout << endl << endl;
		if (!q.empty()) afisare2d();
	}
}

void afisare2d(nod *r)
{
	nrsp = 4 * nrv;
	k = 1;
	while (!q.empty()) q.pop_front();
	lvl = 1;
	if (r)
	{
		q.push_back(r);
		for (int i = 0; i < nrsp; i++)
			cout << " ";
		cout << r->data << endl;
		afisare2d();
	}
	else cout << "Arbore vid!\n";
}


//// CONSTRUIRE AVL

int Eq_Required;
int nr;


void RSS(nod *&rad)
{
	nod *a = rad->dr;
	rad->dr = a->st;
	a->st = rad;
	//rad = a;
	rad->height = 0;
	a->height = 0;
	rad = a;
}
void RSD(nod *&rad)
{
	nod *a;
	a = rad->st;
	rad->st = a->dr;
	a->dr = rad;
	//rad = a;
	rad->height = 0;
	a->height = 0;
	rad = a;
}
void RDS(nod *&rad)
{
	nod *b, *a;
	b = rad->dr;
	a = b->st;
	switch (a->height)
	{
	case 0:
		rad->height = b->height = 0;
		break;
	case -1:
		rad->height = 0;
		b->height = 1;
		break;
	case 1:
		rad->height = -1;
		b->height = 0;
	}
	rad->dr = a->st;
	b->st = a->dr;
	a->st = rad;
	a->dr = b;
	rad = a;
}

void RDD(nod *&rad)
{

	nod *b, *a;
	b = rad->st;
	a = b->dr;
	switch (a->height)
	{
	case 0:
		rad->height = b->height = 0;
		break;
	case -1:
		rad->height = 0;
		b->height = 1;
		break;
	case 1:
		rad->height = -1;
		b->height = 0;
	}
	rad->st = a->dr;
	b->dr = a->st;
	a->height = 0;
	a->dr = rad;
	a->st = b;
	rad = a;
}
void InsAVL(nod *&a, int x)
{

	if (a == 0) //arbore vid, pun radacina
	{
		a = new nod;
		a->data = x; a->st = a->dr = 0; a->height = 0;
		Eq_Required = 1;
	}
	else
	{
		if (x < a->data)
		{ //inserez in stânga ca la BST
			nr++; //asta pentru numararea operatiilor
			InsAVL(a->st, x);
			if (Eq_Required)
				switch (a->height)
				{
				case 1: // adincimea pe subarb stang e mai mare
					Eq_Required = 0;
					//aleg tipul de rotație
					(x < a->st->data) ? RSD(a) : RDD(a);
					break;
				case 0: //nu exista dezechilibru
					a->height = 1; //dar se produce acum
					break;
				case -1: //adincimea pe subarb stang e mai mica
					a->height = 0; // inserarea produce echilibru
					Eq_Required = 0;
				}
		}
		else
			if (x > a->data)
			{
				nr++;
				InsAVL(a->dr, x);
				if (Eq_Required)
					switch (a->height)
					{
					case -1: // adincimea pe subarb drept e mai mare
						Eq_Required = 0;
						//aleg tipul de rotație
						(x > a->dr->data) ? RSS(a) : RDS(a);
						break;
					case 0: //nu exista dezechilibru
						a->height = -1; //dar se produce acum
						break;
					case 1: //adincimea pe subarb stang e mai mica
						a->height = 0; // inserarea produce echilibru
						Eq_Required = 0;
					}

			}
			else Eq_Required = 0;
	}
}