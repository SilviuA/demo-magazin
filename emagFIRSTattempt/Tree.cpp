#include <iostream>
#include <conio.h>
#include "Tree.h"
using namespace std;


int max(int a, int b) /// returns the maximum of two intengers
{
	return (a > b) ? a : b;
}

nod* creare_nod(produs* x)
{
	nod *p = new nod;
	p->data = x;
	p->dr = p->st = 0;
	return p;
}

nod *insert(nod *&r, produs* x)
{
	if (r == 0) return r = creare_nod(x);
	else if (x->getPret() < r->data->getPret())	return insert(r->st, x);
	else if (x->getPret() >= r->data->getPret())  return insert(r->dr, x);
	else return r;
}



nod* removeGreatest(nod*& r)
{
	nod* p;
	if (r->dr == 0) {
		p = r;
		r = r->st;
		return p;
	}
	else return removeGreatest(r->dr);
}

void deleteRoot(nod*& rad)
{
	nod* p = rad;
	if (!rad->st) rad = rad->dr;
	else if (!rad->dr) rad = rad->st;
	else {
		rad = removeGreatest(rad->st);
		rad->st = p->st;
		rad->dr = p->dr;
	}
	delete p;
}

nod* deleteNod2(nod *&rad, produs* x)
{
	if (!rad) return 0;
	else if (x->getPret() < rad->data->getPret()) {
		return deleteNod2(rad->st, x);
	}
	else if (x->getPret() > rad->data->getPret()) {
		return deleteNod2(rad->dr, x);
	}
	else
	{
		if (x != rad->data) return deleteNod2(rad->dr, x);
		else
		{
			 deleteRoot(rad);
			 return rad;
		}
	}
}
nod* deleteNode(nod *&rad, produs* x)
{
	if (!rad)
	{
		cout << "Nu exista produsul!\n";
		return 0;
	}
	return deleteNod2(rad, x);
}

void deleteTree(nod *& rad)
{
	if (rad)
	{
		deleteTree(rad->st);
		deleteTree(rad->dr);
		delete rad;
		rad = 0;
	}
}

int nr2;
nod* aux;
nod* search2(nod* r)
{
	if (r) {

		aux=search2(r->st);
		if (aux) return aux;
		nr2--;
		if (nr2 == 0) return r;
		aux=search2(r->dr);
		if (aux) return aux;
		
	}
	return 0;
}
nod* search(nod *r,int nr)
{
	nr2 = nr;
	if(r)
		return search2(r);
	else {
		cout << "Nu exista produse de acest tip!\n";
		return 0;
	}
}

/********                  *************/
int nrcrt;
void showGreaterOrder2(nod * r, ostream& cout)
{
	if (r)
	{
		showGreaterOrder2(r->st, cout);
		nrcrt++;
		cout << nrcrt << ") ";
		r->data->afisare(cout);
		showGreaterOrder2(r->dr, cout);
	}
}
void showGreaterOrder(nod * r,ostream& cout)
{
	nrcrt = 0;
	showGreaterOrder2(r, cout);
}

void showLowerOrder(nod * r, ostream & cout)
{
	if (r)
	{
		showLowerOrder(r->dr, cout);
		r->data->afisare(cout);
		showLowerOrder(r->st, cout);
	}
}

void descarcaXML2(nod *r, ostream & cout)
{
	if (r)
	{
		descarcaXML2(r->st, cout);
		nrcrt++;
		cout << "\n<nrCRT> " << nrcrt << " </nrCRT>";
		r->data->afisareXML(cout);
		descarcaXML2(r->dr, cout);
	}
}
void descarcaXML(nod * r, ostream & cout)
{
	nrcrt = 0;
	descarcaXML2(r, cout);
}
