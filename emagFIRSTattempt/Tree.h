#pragma once
#include "produse.h"
struct nod
{
	produs *data;
	nod *st, *dr;
};

nod *insert(nod *&r, produs* x);
nod* deleteNode(nod *&rad, produs* x);
void deleteTree(nod *&rad);
nod* search(nod *r, int nr);
void showGreaterOrder(nod* r,ostream& cout);
void showLowerOrder(nod* r, ostream& cout);
void descarcaXML(nod* r, ostream& cout);