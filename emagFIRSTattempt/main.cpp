#include <iostream>
#include <conio.h>
#include "produse.h"
#include "Tree.h"
#include "myStore.h"
#include "Globals.h"
#include "Menu.h"
#include "SimpleOperation.h"
using namespace std;

int main()
{
	Menu *meniu = new Menu("Meniu Principal");
	/**** MENIUL PRODUSELOR ****/
	Menu *submeniuProdus1 = new Menu("Procesor");
	Menu *submeniuProdus2 = new Menu("Display");
	Menu *submeniuProdus3 = new Menu("MemorieRAM");
	Menu *submeniuProdus4 = new Menu("HardDisk");
	Menu *submeniuProdus5 = new Menu("PlacaVideo");
	Menu *submeniuProdus6 = new Menu("Laptop");
	meniu->AddItem(submeniuProdus1);
	meniu->AddItem(submeniuProdus2);
	meniu->AddItem(submeniuProdus3);
	meniu->AddItem(submeniuProdus4);
	meniu->AddItem(submeniuProdus5);
	meniu->AddItem(submeniuProdus6);
	meniu->AddItem(new DescarcaBazaDeDate("Salveaza baza de date"));
	meniu->AddItem(new CitesteBazaDeDate("Citeste baza de date"));
	/****************************************/
	submeniuProdus1->AddItem(new AfisareSTD("Afisare produse (crescator)", 0));
	submeniuProdus1->AddItem(new AfisareDescrescatoare("Afisare produse (descrescator)", 0));
	submeniuProdus1->AddItem(new AdaugaElem("Adauga produs", 0));
	submeniuProdus1->AddItem(new CumparaProdus("Cumpara produs", 0));
	submeniuProdus1->AddItem(new ModificaPret("Modifica pret", 0));
	submeniuProdus1->AddItem(new StergeProdus("Sterge un produs", 0));
	submeniuProdus2->AddItem(new AfisareSTD("Afisare produse (crescator)", 1));
	submeniuProdus2->AddItem(new AfisareDescrescatoare("Afisare produse (descrescator)", 1));
	submeniuProdus2->AddItem(new AdaugaElem("Adauga produs", 1));
	submeniuProdus2->AddItem(new CumparaProdus("Cumpara produs", 1));
	submeniuProdus2->AddItem(new ModificaPret("Modifica pret", 1));
	submeniuProdus2->AddItem(new StergeProdus("Sterge un produs", 1));
	submeniuProdus3->AddItem(new AfisareSTD("Afisare produse (crescator)", 2));
	submeniuProdus3->AddItem(new AfisareDescrescatoare("Afisare produse (descrescator)", 2));
	submeniuProdus3->AddItem(new AdaugaElem("Adauga produs", 2));
	submeniuProdus3->AddItem(new CumparaProdus("Cumpara produs", 2));
	submeniuProdus3->AddItem(new ModificaPret("Modifica pret", 2));
	submeniuProdus3->AddItem(new StergeProdus("Sterge un produs", 2));
	submeniuProdus4->AddItem(new AfisareSTD("Afisare produse (crescator)", 3));
	submeniuProdus4->AddItem(new AfisareDescrescatoare("Afisare produse (descrescator)", 3));
	submeniuProdus4->AddItem(new AdaugaElem("Adauga produs", 3));
	submeniuProdus4->AddItem(new CumparaProdus("Cumpara produs", 3));
	submeniuProdus4->AddItem(new ModificaPret("Modifica pret", 3));
	submeniuProdus4->AddItem(new StergeProdus("Sterge un produs", 3));
	submeniuProdus5->AddItem(new AfisareSTD("Afisare produse (crescator)", 4));
	submeniuProdus5->AddItem(new AfisareDescrescatoare("Afisare produse (descrescator)", 4));
	submeniuProdus5->AddItem(new AdaugaElem("Adauga produs", 4));
	submeniuProdus5->AddItem(new CumparaProdus("Cumpara produs", 4));
	submeniuProdus5->AddItem(new ModificaPret("Modifica pret", 4));
	submeniuProdus5->AddItem(new StergeProdus("Sterge un produs", 4));
	submeniuProdus6->AddItem(new AfisareSTD("Afisare produse (crescator)", 5));
	submeniuProdus6->AddItem(new AfisareDescrescatoare("Afisare produse (descrescator)", 5));
	submeniuProdus6->AddItem(new AdaugaElem("Adauga produs", 5));
	submeniuProdus6->AddItem(new CumparaProdus("Cumpara produs", 5));
	submeniuProdus6->AddItem(new ModificaPret("Modifica pret", 5));
	submeniuProdus6->AddItem(new StergeProdus("Sterge un produs", 5));

	meniu->Execute();

	// intreg arborele de elemente va fi dealocat recursiv
	delete meniu;

	cout << endl << endl << "Sfarsit." << endl;
	Pause();
	return 0;
}